/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define SPI5_CMD_NSS_Pin GPIO_PIN_6
#define SPI5_CMD_NSS_GPIO_Port GPIOF
#define SPI5_CMD_CLK_Pin GPIO_PIN_7
#define SPI5_CMD_CLK_GPIO_Port GPIOF
#define SPI5_CMD_MISO_Pin GPIO_PIN_8
#define SPI5_CMD_MISO_GPIO_Port GPIOF
#define SPI5_CMD_MOSI_Pin GPIO_PIN_9
#define SPI5_CMD_MOSI_GPIO_Port GPIOF
#define GPIO_OUT_ETH_RESET_Pin GPIO_PIN_3
#define GPIO_OUT_ETH_RESET_GPIO_Port GPIOA
#define SPI1_DAT1_CS_Pin GPIO_PIN_4
#define SPI1_DAT1_CS_GPIO_Port GPIOA
#define SPI1_DAT1_SCK_Pin GPIO_PIN_5
#define SPI1_DAT1_SCK_GPIO_Port GPIOA


#define GPIO_OUT_RGB_GREEN_Pin GPIO_PIN_0
#define GPIO_OUT_RGB_GREEN_GPIO_Port GPIOB
#define GPIO_OUT_RGB_BLUE_Pin GPIO_PIN_1
#define GPIO_OUT_RGB_BLUE_GPIO_Port GPIOB
#define GPIO_OUT_RGB_RED_Pin GPIO_PIN_2
#define GPIO_OUT_RGB_RED_GPIO_Port GPIOB


#define ADC_IN_CAN_H_Pin GPIO_PIN_11
#define ADC_IN_CAN_H_GPIO_Port GPIOF
#define ADC_IN_CAN_L_Pin GPIO_PIN_12
#define ADC_IN_CAN_L_GPIO_Port GPIOF
#define ADC_IN_RS485_A_Pin GPIO_PIN_13
#define ADC_IN_RS485_A_GPIO_Port GPIOF
#define ADC_IN_RS485_B_Pin GPIO_PIN_14
#define ADC_IN_RS485_B_GPIO_Port GPIOF
#define SPI1_DAT1_MOSI_Pin GPIO_PIN_5
#define SPI1_DAT1_MOSI_GPIO_Port GPIOB
#define GPIO_OUT_BOOT0_CTRL_Pin GPIO_PIN_7
#define GPIO_OUT_BOOT0_CTRL_GPIO_Port GPIOB

#define UART_RX_MAX_LEN 100
#define UART_RX_BUFF_NUM 3
#define UART_RX_BUFF_SIZE 3

//#define USE_FREERTOS 1
//#define USE_IWDG 1

#define UART_SYNTHETIC_DATA_ENABLE 1
#define USE_SPI_DMA_TX
#define LOWERING_LED_TX_BLINKING 1


#ifdef UART_SYNTHETIC_DATA_ENABLE
    #define UART_BAUD_RATE 		 9600 //31000
		#define UART_FRAME_SEP_BDS 1000 //8 is OK for LAB test //24 is OK for packets 
#else
    #define UART_BAUD_RATE 		 1000000
		#define UART_FRAME_SEP_BDS 24 //8 is OK for LAB test //24 is OK for packets 
#endif


/*
typedef struct{
	physical_rx_data_stream[UART_RX_MAX_LEN]
	
}
*/

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define DSIG_RGB_B_BLUE(z)							 				(z ? (GPIO_OUT_RGB_BLUE_GPIO_Port->BSRRL = GPIO_OUT_RGB_BLUE_Pin) : (GPIO_OUT_RGB_BLUE_GPIO_Port->BSRRH = GPIO_OUT_RGB_BLUE_Pin))
#define DSIG_RGB_B_BLUE_TOGGLE									GPIO_OUT_RGB_BLUE_GPIO_Port->ODR^=(GPIO_OUT_RGB_BLUE_Pin)

#define DSIG_RGB_B_GREEN(z)							 				(z ? (GPIO_OUT_RGB_GREEN_GPIO_Port->BSRRL = GPIO_OUT_RGB_GREEN_Pin) : (GPIO_OUT_RGB_GREEN_GPIO_Port->BSRRH = GPIO_OUT_RGB_GREEN_Pin))
#define DSIG_RGB_B_GREEN_TOGGLE								GPIO_OUT_RGB_GREEN_GPIO_Port->ODR^=(GPIO_OUT_RGB_GREEN_Pin)

#define DSIG_RGB_B_RED(z)							 				(z ? (GPIO_OUT_RGB_RED_GPIO_Port->BSRRL = GPIO_OUT_RGB_RED_Pin) : (GPIO_OUT_RGB_RED_GPIO_Port->BSRRH = GPIO_OUT_RGB_RED_Pin))
#define DSIG_RGB_B_RED_TOGGLE								GPIO_OUT_RGB_RED_GPIO_Port->ODR^=(GPIO_OUT_RGB_RED_Pin)
/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
