/**
  ******************************************************************************
  * @file    stm32h7xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"
#include "stm32h7xx.h"
#include "stm32h7xx_it.h"
#include "cmsis_os.h"
#include <string.h>
#include "main.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
extern DMA_HandleTypeDef hdma_spi1_tx;
extern SPI_HandleTypeDef hspi1;
extern UART_HandleTypeDef huart1;

extern TIM_HandleTypeDef htim17;

extern TIM_HandleTypeDef htim1;
extern int8_t UART_rx_eof;

extern __IO uint8_t UART_RX_buffer[UART_RX_BUFF_NUM][UART_RX_MAX_LEN];
extern __IO uint8_t UART_rx_vector_pointer;
extern __IO uint8_t UART_rx_last_vector_pointer;
 
extern __IO uint8_t UART_RX_len[UART_RX_BUFF_NUM];

extern __IO uint8_t UART_RX_buffer[UART_RX_BUFF_SIZE][UART_RX_MAX_LEN];
extern __IO uint8_t UART_rx_vector_pointer;
extern __IO uint8_t UART_rx_last_vector_pointer;
 
extern __IO uint8_t UART_RX_len[UART_RX_BUFF_SIZE];
extern uint8_t UART_RX_byte;
uint8_t spi_dat_stream[UART_RX_MAX_LEN];

/******************************************************************************/
/*            Cortex Processor Interruption and Exception Handlers         */ 
/******************************************************************************/

/**
* @brief This function handles Non maskable interrupt.
*/
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
* @brief This function handles Hard fault interrupt.
*/
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
  /* USER CODE BEGIN HardFault_IRQn 1 */

  /* USER CODE END HardFault_IRQn 1 */
}

/**
* @brief This function handles Memory management fault.
*/
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
  /* USER CODE BEGIN MemoryManagement_IRQn 1 */

  /* USER CODE END MemoryManagement_IRQn 1 */
}

/**
* @brief This function handles Pre-fetch fault, memory access fault.
*/
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
  /* USER CODE BEGIN BusFault_IRQn 1 */

  /* USER CODE END BusFault_IRQn 1 */
}

/**
* @brief This function handles Undefined instruction or illegal state.
*/
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
  /* USER CODE BEGIN UsageFault_IRQn 1 */

  /* USER CODE END UsageFault_IRQn 1 */
}

/**
* @brief This function handles Debug monitor.
*/
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
* @brief This function handles System tick timer.
*/

#ifndef USE_FREERTOS	

__IO uint8_t it_led_tick = 0;


uint8_t getLedTickCounterFromIT(){
	return(it_led_tick);
}

void resetLedTickCounter(void){
	it_led_tick = 0;
}

uint8_t timer_100ms_tick = 0;

#endif

void SysTick_Handler(void)
{

	
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
	
	#ifdef USE_FREERTOS
	
  osSystickHandler();
	
	#else

	timer_100ms_tick++;
	
	if(timer_100ms_tick>=100){
		timer_100ms_tick=0;
		it_led_tick++;
	}
	
	#endif
	
	
	
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32H7xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32h7xx.s).                    */
/******************************************************************************/

/**
* @brief This function handles DMA1 stream0 global interrupt.
*/
void DMA1_Stream0_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream0_IRQn 0 */

  /* USER CODE END DMA1_Stream0_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_spi1_tx);
  /* USER CODE BEGIN DMA1_Stream0_IRQn 1 */

  /* USER CODE END DMA1_Stream0_IRQn 1 */
}

/**
* @brief This function handles SPI1 global interrupt.
*/
void SPI1_IRQHandler(void)
{
  /* USER CODE BEGIN SPI1_IRQn 0 */

  /* USER CODE END SPI1_IRQn 0 */
  HAL_SPI_IRQHandler(&hspi1);
  /* USER CODE BEGIN SPI1_IRQn 1 */

  /* USER CODE END SPI1_IRQn 1 */
}

/**
* @brief This function handles USB On The Go FS global interrupt.
*/
void OTG_FS_IRQHandler(void)
{
  /* USER CODE BEGIN OTG_FS_IRQn 0 */

  /* USER CODE END OTG_FS_IRQn 0 */
  HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
  /* USER CODE BEGIN OTG_FS_IRQn 1 */

  /* USER CODE END OTG_FS_IRQn 1 */
}

/**
* @brief This function handles TIM17 global interrupt.
*/
void TIM17_IRQHandler(void)
{

  HAL_TIM_IRQHandler(&htim17);

	
}


/**
* @brief This function handles TIM1 capture compare interrupt.
*/
extern __IO uint8_t spi_tx_led_lock;

void TIM1_UP_IRQHandler(void)
{
	__IO uint8_t* pos_pointer = (UART_RX_len+UART_rx_vector_pointer); //pointer to the actual rx buffer stack
	HAL_StatusTypeDef status;
//	static uint16_t licznik_tmp;
//  /* USER CODE BEGIN TIM1_CC_IRQn 0 */
	
	
#ifdef UART_SYNTHETIC_DATA_ENABLE
	
		__IO uint8_t* last_pos_pointer = (UART_RX_len+UART_rx_last_vector_pointer); 
		__IO uint8_t* buffer_pointer = *(UART_RX_buffer+UART_rx_vector_pointer); //pointer to actual len of the stack for incomming data
		__IO uint8_t* last_buffer_pointer = *(UART_RX_buffer+UART_rx_last_vector_pointer);

	HAL_TIM_Base_Stop_IT(&htim1);
	
	sprintf((char*)buffer_pointer, "synthetic-rs485");
	*pos_pointer = 15;
	*(buffer_pointer+15) = 0;
	*(buffer_pointer+16) = 	*pos_pointer;

	status = HAL_SPI_Transmit_DMA(&hspi1, (uint8_t*)last_buffer_pointer, (*last_pos_pointer)+2);
	
	if (status == HAL_OK)
	{	
		TIM1->CNT = status;
	}
	TIM1->CNT = 0;
	
	*last_pos_pointer = 0;
	
#ifndef LOWERING_LED_TX_BLINKING	
	DSIG_RGB_B_GREEN_TOGGLE;
#else
	spi_tx_led_lock=1;
#endif 

	UART_rx_vector_pointer++;
	UART_rx_vector_pointer%=UART_RX_BUFF_NUM;
	UART_rx_last_vector_pointer++;
	UART_rx_last_vector_pointer%=UART_RX_BUFF_NUM;
	
	HAL_TIM_Base_Start_IT(&htim1);
	
	
#else 	
	
	
	if (*pos_pointer>0) //
		{		
			__IO uint8_t* last_pos_pointer = (UART_RX_len+UART_rx_last_vector_pointer); 
			__IO uint8_t* buffer_pointer = *(UART_RX_buffer+UART_rx_vector_pointer); //pointer to actual len of the stack for incomming data
			__IO uint8_t* last_buffer_pointer = *(UART_RX_buffer+UART_rx_last_vector_pointer);
			
			HAL_TIM_Base_Stop_IT(&htim1);
			TIM1->CNT = 0;
			//memcpy(spi_dat_stream, (uint8_t*)last_buffer_pointer, *last_pos_pointer); //that shuld be changed to native rx buffer because of chain solution
			*(last_buffer_pointer+(*last_pos_pointer)+0) = 0x00;
			*(last_buffer_pointer+(*last_pos_pointer)+1) = *pos_pointer; //critical phase - last data are transferred, at new length pointer	

#ifndef USE_SPI_DMA_TX		
			HAL_SPI_Transmit_IT(&hspi1, (uint8_t*)last_buffer_pointer, (*last_pos_pointer)+2); //spi data streaming
#else
			HAL_SPI_Transmit_DMA(&hspi1, (uint8_t*)last_buffer_pointer, (*last_pos_pointer)+2);
#endif

			/*
			memcpy(spi_dat_stream, (uint8_t*)last_buffer_pointer, *last_pos_pointer); //that shuld be changed to native rx buffer because of chain solution (low risk of memory re-write during SPI transfer and UART data collecting)
			*(spi_dat_stream+(*last_pos_pointer)+0) = 0x00;
			*(spi_dat_stream+(*last_pos_pointer)+1) = *pos_pointer; //critical phase - last data are transferred, at new length pointer
			HAL_SPI_Transmit_IT(&hspi1, (uint8_t*)spi_dat_stream, (*last_pos_pointer)+2); //spi data streaming
      */
			
			*last_pos_pointer = 0;
		  DSIG_RGB_B_GREEN_TOGGLE;
			UART_rx_eof = 0;
			
			UART_rx_vector_pointer++;
			UART_rx_vector_pointer%=UART_RX_BUFF_NUM;
			UART_rx_last_vector_pointer++;
			UART_rx_last_vector_pointer%=UART_RX_BUFF_NUM;

	}
		
#endif
	
	HAL_TIM_IRQHandler(&htim1);
		
}

void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */

  /* USER CODE END USART1_IRQn 0 */
  HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */
	
  /* USER CODE END USART1_IRQn 1 */
}
/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
